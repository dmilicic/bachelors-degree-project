package com.fer.dmilicic.tinyos.manager;

public interface TinyOSCommunicationManager {

	public void open();
	public void close();
	
	public byte[] read();
	public void updateNewData(byte[] data);
	
	public void write(byte[] data);
}
