package com.fer.dmilicic.tinyos;

import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import com.fer.dmilicic.tinyos.manager.TinyOsUSBManager;
import com.hoho.android.usbserial.util.HexDump;

public class TinyOSActivity extends Activity {

	private TinyOsUSBManager manager;
	
    private TextView mTitleTextView;
    public static TextView mDumpTextView;
    private ScrollView mScrollView;
    private Button mButton;
    private Button mStartButton;
    private Button mStopButton;

    final Byte[] podaci = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x03, 0x00, 0x0b, 0x41, 0x42, 0x43 };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tiny_os);

        init();


		manager = new TinyOsUSBManager((UsbManager) getSystemService(Context.USB_SERVICE)) {

			@Override
			public void updateNewData(final byte[] data) {

				TinyOSActivity.this.runOnUiThread(new Runnable() {

					@Override
					public void run() {

				        final String message = "Read " + data.length + " bytes: \n"
				                + HexDump.dumpHexString(data) + "\n\n";
				        mDumpTextView.append(message);
				        mScrollView.smoothScrollTo(0, mDumpTextView.getBottom());
					}
				});
			}
		};
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
	}

    private void init() {
        mDumpTextView = (TextView) findViewById(R.id.demoText);
        mScrollView = (ScrollView) findViewById(R.id.demoScroller);
        mStartButton = (Button) findViewById(R.id.startButton);
        mStopButton = (Button) findViewById(R.id.stopButton);
        mButton = (Button) findViewById(R.id.button);

        mButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                new AsyncWriter().execute(podaci);
            }
        });

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.open();
            }
        });

        mStopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                manager.close();
            }
        });
    }

    private class AsyncWriter extends AsyncTask<Byte, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            manager.open();
        }

        @Override
        protected Void doInBackground(Byte... bytes) {
            byte[] podaci = new byte[bytes.length];

            for (int i = 0; i < bytes.length; i++) {
                podaci[i] = bytes[i];
            }
            manager.write(podaci);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            manager.close();
        }
    }
}
